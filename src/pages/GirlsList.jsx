import React, { useState, useEffect} from "react";
import {useNavigate} from "react-router-dom";

const GirlsList = () => {
    const [girls, setGirls] = useState([])
    const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || [])
    const navigate = useNavigate();

    useEffect(() => {
        fetch('http://localhost:3000/girls.json').then(response => response.json()).then(data => setGirls(data))
    }, []);

    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites))
    }, [favorites]);

    const addToFavorites = (girl) => {
        setFavorites([...favorites, girl])
    }

    const goToFav = () => {
        navigate("/Favorites")
    }

    return (
        <div>
            <h1>상품 리스트</h1>
            {girls.map(girl => (
                <div key={girl.id}>
                    <h2>{girl.girlName}</h2>
                    <p>{girl.girlDescription}</p>
                    <button onClick={() => addToFavorites(girl)}>
                        {favorites.find(fav => fav.id === girl.id) ? '찜 취소' : '찜하기'}
                    </button>
                </div>
            ))}
            <h2>찜 목록</h2>
            {favorites.map(girl => (
                <div key={girl.id}>
                    <h3>{girl.girlName}</h3>
                    <p>{girl.girlDescription}</p>
                </div>
            ))}
        </div>
    )
}

export default GirlsList