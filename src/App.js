import './App.css';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import GirlsList from "./pages/GirlsList";
import Favorites from "./pages/Favorites";
function App() {
  return (
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<GirlsList />} />
          <Route path="/favorites" element={<Favorites />} />
        </Routes>
      </BrowserRouter>
  );
}

export default App;
